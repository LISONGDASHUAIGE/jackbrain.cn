<?php
/**
 * Created by PhpStorm.
 * User: 李松大帅哥
 * Date: 2018/7/10
 * Time: 10:00
 */

namespace app\common\model;

class News extends Base{
    protected $pk = 'news_id';
    protected $autoWriteTimestamp = 'datetime';
    protected $auto = ['create_ip'];

    protected function setCreateIpAttr(){
        return request()->ip();
    }

    public function scopeId($query,$news_id){
        $query -> where('news_id','>',$news_id) -> field('news_id');
    }

    public static function newsAdd($title,$content){
        self::create([
            'title' => $title,
            'content' => $content,
            'user_id' => 1
        ]);
    }
}