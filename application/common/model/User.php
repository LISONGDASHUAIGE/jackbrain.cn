<?php
/**
 * Created by PhpStorm.
 * User: 李松大帅哥
 * Date: 2018/7/10
 * Time: 9:58
 */

namespace app\common\model;

class User extends Base{
    protected $pk = 'user_id';
    protected $autoWriteTimestamp = 'datetime';
    public function account(){
        return $this->hasOne('UserAccount');
    }
    public function task(){
        return $this->hasMany('Task');
    }
}