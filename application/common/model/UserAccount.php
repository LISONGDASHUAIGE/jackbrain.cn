<?php
/**
 * Created by PhpStorm.
 * User: 李松大帅哥
 * Date: 2018/7/10
 * Time: 11:00
 */
namespace app\common\model;

class UserAccount extends Base{
    protected $pk = 'account_id';
    protected $autoWriteTimestamp = 'datetime';

    public function user(){
        return $this->belongsTo('User');
    }

    public static function accountAdd(){
        self::create([
            'money' => 500,
            'user_id' => 1
        ]);
    }
}