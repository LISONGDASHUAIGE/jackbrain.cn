<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 多维数组排序
 * @param $data
 * @param $sort_order_field
 * @param int $sort_order
 * @param int $sort_type
 * @return mixed
 * @author:李松大帅哥
 * @date:2018/7/10 15:38
 */
function array_multi_sort($data, $sort_order_field, $sort_order = SORT_DESC, $sort_type = SORT_NUMERIC){
    foreach ($data as $val) {
        $key_arrays[] = $val[$sort_order_field];
    }
    array_multisort($key_arrays, $sort_order, $sort_type, $data);
    return $data;
}

/**
 * 二维数组排序
 * @param $arr
 * @param $keys
 * @param string $type
 * @return array
 * @author:李松大帅哥
 * @date:2018/7/10 16:10
 */
function array_sort($arr, $keys, $type = 'desc'){
    $key_value = $new_array = array();
    foreach ($arr as $k => $v) {
        $key_value[$k] = $v[$keys];
    }
    if ($type == 'asc') {
        asort($key_value);
    } else {
        arsort($key_value);
    }
    reset($key_value);
    foreach ($key_value as $k => $v) {
        $new_array[$k] = $arr[$k];
    }
    return $new_array;
}

/**
 * 多维数组转化为一维数组
 * @param $array
 * @return array
 * @author:李松大帅哥
 * @date:2018/7/10 16:10
 */
function array_multi2single($array){
    static $result_array = array();
    foreach ($array as $value) {
        if (is_array($value)) {
            array_multi2single($value);
        } else
            $result_array [] = $value;
    }
    return $result_array;
}

/**
 * base64图片解码，默认为POST上传
 * @param $base64
 * @param int $need_slashes
 * @return bool|string
 * @author:李松大帅哥
 * @date:2018/7/10 15:39
 */
function base64_upload($base64,$need_slashes = 1){
    $base64_image = str_replace(' ', '+', $base64);
    //post的数据里面，加号会被替换为空格，需要重新替换回来，如果不是post的数据，则注释掉这一行
    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image, $result)) {
        //匹配成功
        if ($result[2] == 'jpeg') {
            $image_name = uniqid() . '.jpg';
            //纯粹是看jpeg不爽才替换的
        } else {
            $image_name = uniqid() . '.' . $result[2];
        }
        $image_file = "./upload/car/{$image_name}";
        //服务器文件存储路径
        if (file_put_contents($image_file, base64_decode(str_replace($result[1], '', $base64_image)))) {
            if ($need_slashes) {
                return "/upload/car/{$image_name}";
            }
            return "upload/car/{$image_name}";
        } else {
            return false;
        }
    } else {
        return false;
    }
}

/**
 * 循环删除指定目录下的文件及文件夹
 * @param $dirpath
 * @return bool
 * @author:李松大帅哥
 * @date:2018/7/10 15:42
 */
function del_dir($dirpath){
    $dh = opendir($dirpath);
    while (($file = readdir($dh)) !== false) {
        if ($file != "." && $file != "..") {
            $fullpath = $dirpath . "/" . $file;
            if (! is_dir($fullpath)) {
                unlink($fullpath);
            } else {
                del_dir($fullpath);
                rmdir($fullpath);
            }
        }
    }
    closedir($dh);
    $isEmpty = true;
    $dh = opendir($dirpath);
    while (($file = readdir($dh)) !== false) {
        if ($file != "." && $file != "..") {
            $isEmpty = false;
            break;
        }
    }
    return $isEmpty;
}

/**
 * 判断当前访问的用户是  PC端  还是 手机端  返回true 为手机端  false 为PC 端
 * @return bool
 * @author:李松大帅哥
 * @date:2018/7/10 16:24
 */
function isMobile(){
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return true;

    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
    {
        // 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT']))
    {
        $clientkeywords = array ('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile');
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT'])))
            return true;
    }
    // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT']))
    {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        }
    }
    return false;
}

/**
 * 判断当前是否是微信浏览器 QQ 支付宝
 * @return int
 * @author:李松大帅哥
 * @date:2018/7/10 15:42
 */
function is_wei_xin(){
    if (strpos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger') !== false) {
        return 1;
    }
    return 0;
}
function is_qq() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'QQ') !== false) {
        return true;
    } return false;
}
function is_ali_pay() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'AlipayClient') !== false) {
        return true;
    } return false;
}

/**
 * 把微信生成的图片存入本地
 * @param $local_path
 * @param $weixin_path
 * @return mixed
 * @author:李松大帅哥
 * @date:2018/7/10 15:52
 */
function save_wei_xin_img($local_path, $weixin_path){
    $weixin_path_a = str_replace("https://", "http://", $weixin_path);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $weixin_path_a);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $r = curl_exec($ch);
    curl_close($ch);
    if (! empty($local_path) && ! empty($weixin_path_a)) {
        file_put_contents($local_path, $r);
    }
    return $local_path;
}

/**
 * 请求远程地址
 * @param $url
 * @param int $timeout
 * @param array $header
 * @return mixed
 * @throws Exception
 * @author:李松大帅哥
 * @date:2018/7/10 15:55
 */
function http($url, $timeout = 30, $header = array()){
    if (! function_exists('curl_init')) {
        throw new Exception('server not install curl');
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    if (! empty($header)) {
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    }
    $data = curl_exec($ch);
    list ($header, $data) = explode("\r\n\r\n", $data);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($http_code == 301 || $http_code == 302) {
        $matches = array();
        preg_match('/Location:(.*?)\n/', $header, $matches);
        $url = trim(array_pop($matches));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $data = curl_exec($ch);
    }

    if ($data == false) {
        curl_close($ch);
    }
    @curl_close($ch);
    return $data;
}


/**
 * CURL请求
 * @param $url string 请求url地址
 * @param $method string 请求方法 get post
 * @param $postfields string post数据数组
 * @param $headers array 请求header信息
 * @param $debug bool|false 调试开启 默认false
 * @return mixed
 */
function httpRequest($url, $method="GET", $postfields = null, $headers = array(), $debug = false) {
    $method = strtoupper($method);
    $ci = curl_init();
    /* Curl settings */
    curl_setopt($ci, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    curl_setopt($ci, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0");
    curl_setopt($ci, CURLOPT_CONNECTTIMEOUT, 60); /* 在发起连接前等待的时间，如果设置为0，则无限等待 */
    curl_setopt($ci, CURLOPT_TIMEOUT, 7); /* 设置cURL允许执行的最长秒数 */
    curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
    switch ($method) {
        case "POST":
            curl_setopt($ci, CURLOPT_POST, true);
            if (!empty($postfields)) {
                $tmpdatastr = is_array($postfields) ? http_build_query($postfields) : $postfields;
                curl_setopt($ci, CURLOPT_POSTFIELDS, $tmpdatastr);
            }
            break;
        default:
            curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method); /* //设置请求方式 */
            break;
    }
    $ssl = preg_match('/^https:\/\//i',$url) ? TRUE : FALSE;
    curl_setopt($ci, CURLOPT_URL, $url);
    if($ssl){
        curl_setopt($ci, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
        curl_setopt($ci, CURLOPT_SSL_VERIFYHOST, FALSE); // 不从证书中检查SSL加密算法是否存在
    }
    //curl_setopt($ci, CURLOPT_HEADER, true); /*启用时会将头文件的信息作为数据流输出*/
    curl_setopt($ci, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ci, CURLOPT_MAXREDIRS, 2);/*指定最多的HTTP重定向的数量，这个选项是和CURLOPT_FOLLOWLOCATION一起使用的*/
    curl_setopt($ci, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ci, CURLINFO_HEADER_OUT, true);
    /*curl_setopt($ci, CURLOPT_COOKIE, $Cookiestr); * *COOKIE带过去** */
    $response = curl_exec($ci);
    $requestinfo = curl_getinfo($ci);
    $http_code = curl_getinfo($ci, CURLINFO_HTTP_CODE);
    if ($debug) {
        echo "=====post data======\r\n";
        var_dump($postfields);
        echo "=====info===== \r\n";
        print_r($requestinfo);
        echo "=====response=====\r\n";
        print_r($response);
    }
    curl_close($ci);
    return $response;
    //return array($http_code, $response,$requestinfo);
}

/**
 * 去掉小数后面的0，整数的话要去掉.
 * @param $arg
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 15:56
 */
function del_zero($arg){
    return rtrim(rtrim($arg,'0'),'.');
}

/**
 * 得到相差时间的中文
 * @param $time
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 15:57
 */
function time_range($time){
    $num = time()-$time;
    $range = array(60,3600,86400,2592000,31536000);
    if($num<$range[0]){
        $time = $num."秒";
    }elseif ($num>$range[0] && $num<$range[1]){
        $branch = floor($num/$range[0]);
        $time = $branch."分钟";
    }elseif ($num>$range[1] && $num<$range[2]){
        $hour = floor($num/$range[1]);
        $time = $hour."小时";
    }elseif ($num>$range[2] && $num<$range[3]){
        $day = floor($num/$range[2]);
        $time = $day."天";
    }elseif ($num>$range[3] && $num<$range[4]){
        $month = floor($num/$range[3]);
        $time = $month>10?$month."月":$month."个月";
    }elseif ($num>$range[4]){
        $year = floor($num/$range[4]);
        $time = $year."年";
    }
    return $time."之前";
}

/**
 * 求两个已知经纬度之间的距离,单位为米
 * @param $lng1
 * @param $lat1
 * @param $lng2
 * @param $lat2
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 16:00
 */
function count_distance($lng1, $lat1, $lng2, $lat2) {
    // 将角度转为狐度
    $radLat1 = deg2rad(floatval($lat1)); //deg2rad()函数将角度转换为弧度
    $radLat2 = deg2rad(floatval($lat2));
    $radLng1 = deg2rad(floatval($lng1));
    $radLng2 = deg2rad(floatval($lng2));
    //104.116791,30.662557,104.113882,30.662421
    $a = $radLat1 - $radLat2;
    $b = $radLng1 - $radLng2;
    $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
    return $s;
}

/**
 * 获取url 中的各个参数  类似于 pay_code=alipay&bank_code=ICBC-DEBIT
 * @param $str
 * @return array
 * @author:李松大帅哥
 * @date:2018/5/31 11:11
 */
function parse_url_param($str){
    $data = array();
    $str = explode('?',$str);
    $str = end($str);
    $parameter = explode('&',$str);
    foreach($parameter as $val){
        $tmp = explode('=',$val);
        $data[$tmp[0]] = $tmp[1];
    }
    return $data;
}

/**
 * 将数据库中查出的列表以指定的 id 作为数组的键名
 * @param $arr
 * @param $key_name
 * @return array
 * @author:李松大帅哥
 * @date:2018/7/10 16:08
 */
function convert_arr_key($arr, $key_name)
{
    $arr2 = array();
    foreach($arr as $key => $val){
        $arr2[$val[$key_name]] = $val;
    }
    return $arr2;
}

/**
 * 获取数组中的某一列
 * @param $arr
 * @param $key_name
 * @return array
 * @author:李松大帅哥
 * @date:2018/7/10 16:08
 */
function get_arr_column($arr, $key_name){
    $arr2 = array();
    foreach($arr as $key => $val){
        $arr2[] = $val[$key_name];
    }
    return $arr2;
}

/**
 * 友好时间显示
 * @param $time
 * @return bool|false|string
 * @author:李松大帅哥
 * @date:2018/7/10 16:11
 */
function friend_date($time){
    if (!$time) return false;
    $d = time() - intval($time);
    $ld = $time - mktime(0, 0, 0, 0, 0, date('Y')); //得出年
    $md = $time - mktime(0, 0, 0, date('m'), 0, date('Y')); //得出月
    $byd = $time - mktime(0, 0, 0, date('m'), date('d') - 2, date('Y')); //前天
    $yd = $time - mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')); //昨天
    $dd = $time - mktime(0, 0, 0, date('m'), date('d'), date('Y')); //今天
    $td = $time - mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')); //明天
    $atd = $time - mktime(0, 0, 0, date('m'), date('d') + 2, date('Y')); //后天
    if ($d == 0) {
        $fdate = '刚刚';
    } else {
        switch ($d) {
            case $d < $atd:
                $fdate = date('Y年m月d日', $time);
                break;
            case $d < $td:
                $fdate = '后天' . date('H:i', $time);
                break;
            case $d < 0:
                $fdate = '明天' . date('H:i', $time);
                break;
            case $d < 60:
                $fdate = $d . '秒前';
                break;
            case $d < 3600:
                $fdate = floor($d / 60) . '分钟前';
                break;
            case $d < $dd:
                $fdate = floor($d / 3600) . '小时前';
                break;
            case $d < $yd:
                $fdate = '昨天' . date('H:i', $time);
                break;
            case $d < $byd:
                $fdate = '前天' . date('H:i', $time);
                break;
            case $d < $md:
                $fdate = date('m月d日 H:i', $time);
                break;
            case $d < $ld:
                $fdate = date('m月d日', $time);
                break;
            default:
                $fdate = date('Y年m月d日', $time);
                break;
        }
    }
    return $fdate;
}

/**
 * 得到服务器端IP
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 16:12
 */
function get_server_ip(){
    return gethostbyname($_SERVER["SERVER_NAME"]);
}

/**
 * 两个数组的笛卡尔积
 * @param $arr1
 * @param $arr2
 * @return array
 * @author:李松大帅哥
 * @date:2018/7/10 16:13
 */
function descartes_array($arr1,$arr2) {
    $result = array();
    foreach ($arr1 as $item1)
    {
        foreach ($arr2 as $item2)
        {
            $temp = $item1;
            $temp[] = $item2;
            $result[] = $temp;
        }
    }
    return $result;
}

/**
 * 多个数组的笛卡尔积
 * @return array
 * @author:李松大帅哥
 * @date:2018/7/10 16:12
 */
function descartes_array_m() {
    $data = func_get_args();
    $data = current($data);
    $cnt = count($data);
    $result = array();
    $arr1 = array_shift($data);
    foreach($arr1 as $key=>$item)
    {
        $result[] = array($item);
    }

    foreach($data as $key=>$item)
    {
        $result = descartes_array($result,$item);
    }
    return $result;
}

/**
 * 获取随机字符串
 * @param int $randLength  长度
 * @param int $addTime  是否加入当前时间戳
 * @param int $includeNumber   是否包含数字
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 16:12
 */
function get_rand_str($randLength=6,$addTime=1,$includeNumber=0){
    if ($includeNumber){
        $chars='abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQEST123456789';
    }else {
        $chars='abcdefghijklmnopqrstuvwxyz';
    }
    $len=strlen($chars);
    $randStr='';
    for ($i=0;$i<$randLength;$i++){
        $randStr.=$chars[rand(0,$len-1)];
    }
    $tokenvalue=$randStr;
    if ($addTime){
        $tokenvalue=$randStr.time();
    }
    return $tokenvalue;
}

/**
 * 过滤数组元素前后空格 (支持多维数组)
 * @param $array
 * @return array|string
 * @author:李松大帅哥
 * @date:2018/7/10 16:20
 */
function trim_array_element($array){
    if(!is_array($array))
        return trim($array);
    return array_map('trim_array_element',$array);
}

/**
 * 检查手机号码格式
 * @param $mobile
 * @return bool
 * @author:李松大帅哥
 * @date:2018/7/10 16:20
 */
function check_mobile($mobile){
    if(preg_match('/1[34578]\d{9}$/',$mobile))
        return true;
    return false;
}

/**
 * 检查固定电话
 * @param $mobile
 * @return bool
 * @author:李松大帅哥
 * @date:2018/7/10 16:20
 */
function check_telephone($mobile){
    if(preg_match('/^([0-9]{3,4}-)?[0-9]{7,8}$/',$mobile))
        return true;
    return false;
}

/**
 * 检查邮箱地址格式
 * @param $email
 * @return bool
 * @author:李松大帅哥
 * @date:2018/7/10 16:21
 */
function check_email($email){
    if(filter_var($email,FILTER_VALIDATE_EMAIL))
        return true;
    return false;
}

/**
 * 中文字串截取
 * @param $string
 * @param $start
 * @param $length
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 16:22
 */
function getSubstr($string, $start, $length) {
    if(mb_strlen($string,'utf-8')>$length){
        $str = mb_substr($string, $start, $length,'utf-8');
        return $str.'...';
    }else{
        return $string;
    }
}

/**
 * 获取中文字符拼音首字母
 * @param $str
 * @return null|string
 * @author:李松大帅哥
 * @date:2018/7/10 16:25
 */
function getFirstCharter($str){
    if(empty($str))
    {
        return '';
    }
    $fchar=ord($str{0});
    if($fchar>=ord('A')&&$fchar<=ord('z')) return strtoupper($str{0});
    $s1=iconv('UTF-8','gb2312',$str);
    $s2=iconv('gb2312','UTF-8',$s1);
    $s=$s2==$str?$s1:$str;
    $asc=ord($s{0})*256+ord($s{1})-65536;
    if($asc>=-20319&&$asc<=-20284) return 'A';
    if($asc>=-20283&&$asc<=-19776) return 'B';
    if($asc>=-19775&&$asc<=-19219) return 'C';
    if($asc>=-19218&&$asc<=-18711) return 'D';
    if($asc>=-18710&&$asc<=-18527) return 'E';
    if($asc>=-18526&&$asc<=-18240) return 'F';
    if($asc>=-18239&&$asc<=-17923) return 'G';
    if($asc>=-17922&&$asc<=-17418) return 'H';
    if($asc>=-17417&&$asc<=-16475) return 'J';
    if($asc>=-16474&&$asc<=-16213) return 'K';
    if($asc>=-16212&&$asc<=-15641) return 'L';
    if($asc>=-15640&&$asc<=-15166) return 'M';
    if($asc>=-15165&&$asc<=-14923) return 'N';
    if($asc>=-14922&&$asc<=-14915) return 'O';
    if($asc>=-14914&&$asc<=-14631) return 'P';
    if($asc>=-14630&&$asc<=-14150) return 'Q';
    if($asc>=-14149&&$asc<=-14091) return 'R';
    if($asc>=-14090&&$asc<=-13319) return 'S';
    if($asc>=-13318&&$asc<=-12839) return 'T';
    if($asc>=-12838&&$asc<=-12557) return 'W';
    if($asc>=-12556&&$asc<=-11848) return 'X';
    if($asc>=-11847&&$asc<=-11056) return 'Y';
    if($asc>=-11055&&$asc<=-10247) return 'Z';
    return null;
}

/**
 * 获取整条字符串汉字拼音首字母
 * @param $zh
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 16:26
 */
function pinyin_long($zh){
    $ret = "";
    $s1 = iconv("UTF-8","gb2312", $zh);
    $s2 = iconv("gb2312","UTF-8", $s1);
    if($s2 == $zh){$zh = $s1;}
    for($i = 0; $i < strlen($zh); $i++){
        $s1 = substr($zh,$i,1);
        $p = ord($s1);
        if($p > 160){
            $s2 = substr($zh,$i++,2);
            $ret .= getFirstCharter($s2);
        }else{
            $ret .= $s1;
        }
    }
    return $ret;
}

function url_safe_b64encode($string){
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_',''),$data);
    return $data;
}

/**********************************************************************************************************************/

/**
 * 获取标准二维码格式
 * @param $url
 * @param $path
 * @param $code_name
 * @return string
 * @author:李松大帅哥
 * @date:2018/7/10 16:50
 */
function get_qr_code($url, $path, $code_name){
    if (! is_dir($path)) {
        $mode = intval('0777', 8);
        mkdir($path, $mode, true);
        chmod($path, $mode);
    }
    $path = $path . '/' . $code_name . '.png';
    if(file_exists($path)){
        unlink($path);
    }
    \data\extend\QRcode::png($url, $path, '', 4, 1);
    return $path;
}