<?php
/**
 * Created by PhpStorm.
 * User: Song Li
 * Date: 2018/7/1
 * Time: 16:23
 */

namespace app\index\validate;

use think\Validate;

class News extends Validate{

    protected $rule = [
        'title' => 'require',
        'content' => 'require',
    ];

    protected $message = [
        'title.require' => '文章标题必须',
        'content.require' => '文章内容必须',
    ];
}