<?php
/**
 * Created by PhpStorm.
 * User: Song Li
 * Date: 2018/7/1
 * Time: 15:47
 */

namespace app\index\controller;

use think\Request;

/**
 * 空控制器，当访问不存在的控制器时会访问这里
 * Class Error
 * @package app\index\controller
 * @author 李松大帅哥
 * @date 2018/7/1 16:02
 */
class Error extends Base{
    public function index(Request $request){
        $name = $request -> controller();
        return $name;
    }
}