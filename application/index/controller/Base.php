<?php
/**
 * Created by PhpStorm.
 * User: Song Li
 * Date: 2018/7/1
 * Time: 15:41
 */

namespace app\index\controller;

use think\Controller;

class Base extends Controller{

    /**
     * 空方法，当访问未定义的方法时会访问这里
     * @param $name
     * @return mixed
     * @author 李松大帅哥
     * @date 2018/7/1 15:58
     */
    public function _empty($name){
        return $name;
    }
}