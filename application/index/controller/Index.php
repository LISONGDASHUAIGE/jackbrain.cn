<?php

namespace app\index\controller;

use app\common\model\User;
use app\common\model\UserAccount;
use Redis\Redis;
use think\Image;

class Index extends Base
{

    public function index(){
        echo 'hello world!';
    }

    /**
     * 图片处理
     * @author:李松大帅哥
     * @date:2018/7/10 15:34
     */
    public function index5(){
        $src = "./fruit.png";
        $image = Image::open($src);
        $image -> thumb(150,150,3) -> save('./thumb.png');
        $image -> crop(300,300,100,200) -> save('./copy.png');
    }

    /**
     * 生产消费者队列
     * @author:李松大帅哥
     * @date:2018/7/10 15:34
     */
    public function index4(){
        $a = Redis::getInstance()->getConnect()->lPop('task_list');
        halt($a);

        $a = Redis::getInstance()->getConnect()->rPush('task_list','task '.date('Y-m-d H:i:s'));
        halt($a);
    }

    public function index3(){
        $user = User::get(12);
        $user->task()->save(['task_id'=>10,'task_name'=>'task66']);
    }

    /**
     * 关联自动写入
     * @author:李松大帅哥
     * @date:2018/7/10 11:35
     */
    public function index2(){
        $user = new User();
        $user -> user_name = '李松大帅哥1';
        $user -> password = 'e10adc3949ba59abbe56e057f20f883e';
        $account = new UserAccount();
        $account -> money = 5000;
        $user->account = $account;
        $user->together('account')->save();die;
    }

    /**
     * 关联自动删除 ，好像有个Bug，如果在User模型里面绑定属性到父模型后，即在hasOne方法后调用bind，无法关联删除
     * @author:李松大帅哥
     * @date:2018/7/10 11:45
     */
    public function index1(){
        $user = User::get(14,'account');
        $user->together('account')->delete();
    }
}
