<?php
/**
 * Created by PhpStorm.
 * User: 李松大帅哥
 * Date: 2018/6/29
 * Time: 13:51
 */
namespace app\index\controller;


class News extends Base {

    protected $batchValidate = true;

    /**
     * @param $name
     * @author:李松大帅哥
     * @date:2018/6/30 10:50
     * @route('write/:name')
     */
    public function write($name){
        echo $name;
    }

    public function test(){
        return 1;
    }

    public function index(){

        $data = [
            'title' => '',
            'content' => '1',
        ];

        $result = $this->validate($data,'app\index\validate\News');

        halt($result);

        return $this->request->action();
    }

    public function create(){
        return $this->request->action();
    }

    public function save(){
        return $this->request->action();
    }

    public function read($id){
        return $this->request->action() .' '. $id;
    }

    public function edit($id){
        return $this->request->action() .' '. $id;
    }

    public function update($id){
        return $this->request->action() .' '. $id;
    }

    public function delete($id){
        return $this->request->action() .' '. $id;
    }
}