<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::rule('/','index/index/index')->allowCrossDomain();

Route::rule('hello/:name',function ($name){
    return 'Hello' . $name;
});

Route::rule('getIp',function (\think\Request $request){
    $method = $request -> domain();
    return $method;
});

Route::rule('response',response()
    ->data('hello')
    ->code(200)
    ->contentType('text/plain')
);

Route::resource('news','index/News');

Route::resource('user','index/User');

Route::miss(function (){
    return 'you seemed lost at our website!';
});


return [

];